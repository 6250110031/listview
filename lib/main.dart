import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // const MyApp({Key? key}) : super(key: key);

  final titles = [
    'bike',
    'boat',
    'bus',
    'car',
    'railway',
    'run',
    'subway',
    'transit',
    'walk'
  ];
  final images = [
    NetworkImage('https://cdn-icons-png.flaticon.com/512/3198/3198344.png'),
    NetworkImage(
        'https://cdn-icons-png.flaticon.com/512/4001/4001871.png'),
    NetworkImage(
        'https://cdn-icons-png.flaticon.com/512/635/635705.png'),
    NetworkImage(
        'https://cdn-icons.flaticon.com/png/512/3054/premium/3054929.png?token=exp=1642753197~hmac=645bd7750f30f66a27c21c9b51484a96'),
    NetworkImage(
        'https://cdn-icons-png.flaticon.com/512/1185/1185568.png'),
    NetworkImage('https://cdn-icons.flaticon.com/png/512/1267/premium/1267761.png?token=exp=1642753262~hmac=a71ebcda13afb206ee6ebf43ec7f6d20'),
    NetworkImage(
        'https://as1.ftcdn.net/v2/jpg/03/12/17/88/1000_F_312178834_aQP0ufNYoUJv4InGtC7NF2qxZI9cNZyn.jpg'),
    NetworkImage(
        'https://as2.ftcdn.net/v2/jpg/01/01/41/99/1000_F_101419995_v8EYd2Z5PSLRSvY6ufOcNdcoEyrIyH8k.jpg'),
    NetworkImage(
        'https://previews.123rf.com/images/martialred/martialred1812/martialred181200030/113393191-una-persona-caminando-o-caminando-signo-icono-de-vector-plano-para-aplicaciones-y-sitios-web.jpg?fj=1'),
  ];

  // final icons = [
  //   Icons.directions_bike,
  //   Icons.directions_boat,
  //   Icons.directions_bus,
  //   Icons.directions_car,
  //   Icons.directions_railway,
  //   Icons.directions_run,
  //   Icons.directions_subway,
  //   Icons.directions_transit,
  //   Icons.directions_walk
  // ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Basic ListView',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
          appBar: AppBar(
            title: Text('List View'),
          ),
          body: ListView.builder(
            itemCount: titles.length,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                children: [
                  ListTile(
                    leading: CircleAvatar(
                      backgroundImage: images[index],
                    ),
                    title: Text(
                      '${titles[index]}',
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text(
                      'There are many passengers in serveral vehicles',
                      style: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                    trailing: Icon(
                      Icons.notifications_none,
                      size: 25,
                    ),
                    onTap: () {
                      AlertDialog alert = AlertDialog(
                        title: Text(
                            'Welcome'), // To display the title it is optional
                        content: Text(
                            'This is a ${titles[index]}'), // Message which will be pop up on the screen
                        // Action widget which will provide the user to acknowledge the choice
                        actions: [
                          ElevatedButton(
                            // FlatButton widget is used to make a text to work like a button

                            onPressed: () {
                              Navigator.of(context).pop();
                            }, // function used to perform after pressing the button
                            child: Text('CANCEL'),
                          ),
                          ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text('ACCEPT'),
                          ),
                        ],
                      );
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return alert;
                        },
                      );
                    },
                  ),
                  Divider(
                    thickness: 0.8,
                  ),
                ],
              );
            },
          )),
    );
  }
}
